import React from "react";
import { Routes, Route, BrowserRouter } from "react-router-dom";

import Home from "./pages/Home";
import Sobre from "./pages/Sobre";
import Compromissos from "./pages/Compromissos";
import Header from "./Components/Header";

import Perfil from "../src/assets/perfil.jpeg";
import Agenda from "./pages/Agenda";
import Login from "./pages/Login";
import CadastroUsuario from "./pages/CadastroUsuarios";


const user = {
  name: "Ana Avelino",
  email: "ana@example.com",
  imageUrl: Perfil,
};
const navigation = [
  { name: "Página Inicial", to: "/", current: false },
  { name: "Compromissos", to: "/compromissos", current: false },
  { name: "Agenda", to: "/agenda", current: false },
];
const userNavigation = [
  { name: "Seu perfil", to: "#" },
  { name: "Configurações", to: "#" },
  { name: "Sair", to: "#" },
];

const Rotas = () => {
  return (
    <BrowserRouter>
        <Header
          user={user}
          navigation={navigation}
          userNavigation={userNavigation}
        />
      <Routes>
        <Route element={<Home />} path="/" exact />
        <Route element={<Sobre />} path="/sobre" />
        <Route element={<Compromissos />} path="/compromissos" />
        <Route element={<Agenda />} path="/agenda" />

        <Route element={<Login/>} path="/login"/>
        <Route element={<CadastroUsuario/>} path="/cad-usuario"/>


      </Routes>
    </BrowserRouter>
  );
};

export default Rotas;
