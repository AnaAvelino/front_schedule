import React from "react";
import FormControlled from "./Components/FormControlled/index";
// import './App.css';
// import Welcome from "./Components/Welcome/index";
import Rotas from "./routes";
require('dotenv').config()


function App() {
  return (
    <div className="bg-gray-100 h-screen">
      <Rotas />
    </div>

      );
    }
    
    // {/* <FormControlled/> */}
    // {/* <Welcome name='Ana'></Welcome> */}
export default App;
