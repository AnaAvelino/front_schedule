
import axios from 'axios';

const api = axios.create({
	timeout: 1000,
	headers: { "Content-Type": "application/json" },
	baseURL: "http://localhost:3000/"
});

// process.env.REACT_APP_API_URL,
export default api;