import React from "react";

export default function Button({type, title}) {
  return (
    <button
      className="w-full bg-purple-700 rounded-md h-14 text-gray-100 font-semibold hover:bg-purple-600 text-lg"
      type={type}
    >
      {/* {title} */} {title}
    </button>
  );
}
