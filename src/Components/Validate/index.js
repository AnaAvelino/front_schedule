import * as yup from 'yup';


const rePhoneNumber = '/^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/';
export const Formvalidations = yup.object().shape({
    name: yup
        .string()
        .required("Campo Nome é obrigatorio"),

    setor:yup
        .string()
        .required("Campo Setor é obrigatorio"),
    
    telefone:yup
        .string()
        .matches("Numero de Telefone invalido")

    
})

