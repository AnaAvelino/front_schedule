import Input from "../Input/Input"
import React, {useEffect, useState} from 'react';
import { Formvalidations } from "../Validate";
import { ValidationError } from "yup";

const inicialFormState = {
    name:'',
    setor:'',
    telefone1:'',
}

const UserForm = () => {
    
    const [form,setForm] = useState(inicialFormState)
    const [errors, setErros] = useState({})

    const validate = async () => {
        try {
            await Formvalidations.validate(form, {abortEarly: false})
            setErros({})
        }catch (e) {
            if(e instanceof ValidationError) {
                const errors = {}
                e.inner.forEach((key) => {
                    errors[key.path] = key.message
                })
                    setErros(errors)
            }
        }
    }

    const setInput = (newValue)=> {
        setForm(form => ({...form, ...newValue}))
    }

    useEffect(() => {validate()},[form])

    return(
        <>
            <h3>Cadastro de Contatos</h3>
                <form>
                    <div className="form-group">
                        <Input
                            name="name"
                            onChange={e => setInput({name: e.target.value})}
                            label="Name"
                            error={errors.name}
                        />

                    </div>
                    <div className="form-group">
                        <Input
                            name="setor"
                            onChange={e => setInput({setor: e.target.value})}
                            label="Setor"
                            error={errors.setor}
                        />
                    </div>
                    <div className="form-group">
                        <Input
                            name="telefone"
                            onChange={e => setInput({telefone: e.target.value})}
                            label="Telefone"
                            error={errors.telefone}

                        />
                    </div>
                    <div className="form-group">
                        <button type="button" className="btn btn-primary">Salvar</button>
                    </div>
                </form>
        </>
    )
}

export default UserForm;