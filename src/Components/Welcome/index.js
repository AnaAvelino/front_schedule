// import React, { useState } from "react";
// import Header from "../Header/index";
// // import Calendario from '../useCalendario/index'
// // import Calendario from "react-cale";
// // import "react-calendar/dist/Calendar.css";
// import Perfil from "../../assets/perfil.jpeg";
// import ImgCard from "../../assets/cardImg.png";
// import Homefundo from "../../assets/fundoHome2.jpg";

// const user = {
//   name: "Ana Avelino",
//   email: "ana@example.com",
//   imageUrl: Perfil,
// };
// const navigation = [
//   { name: "Página Inicial", href: "#", current: false },
//   { name: "Compromissos", href: "#", current: false },
//   { name: "Calendario", href: "#", current: false },
// ];
// const userNavigation = [
//   { name: "Seu perfil", href: "#" },
//   { name: "Configurações", href: "#" },
//   { name: "Sair", href: "#" },
// ];

// function classNames(...classes) {
//   return classes.filter(Boolean).join("");
// }

// export default function Home() {
//   const [value, onChange] = useState(new Date());
//   // const Perfil = require("../../../public/perfil.jpeg");

//   return (
//     <div className="">
//       <Header
//         user={user}
//         navigation={navigation}
//         userNavigation={userNavigation}
//       />
//       <div className=" ">
//         <div class="flex flex-col w-screen  ">
//           <div
//             className="text-center py-32 h-5/6 relative"
//             style={{ backgroundImage: `url(${Homefundo})` }}
//           >
//             <div className="backdrop-opacity-60 backdrop-invert bg-white/30">
//               <h1 className="text-4xl  font-semibold mt-10 opacity-100">
//                 My Schedule
//               </h1>
//               <div className="h-4"></div>
//               <p className="">
//                 Fique a vontade para conhecer funcionalidades que o(a) ajudaram
//                 a organizar suas atividade diaria de um forma pratica e rápida
//               </p>
//               <div className="h-2"></div>
//               <button className="rounded-lg bg-purple-900 text-gray-200 w-36 h-10 ">
//                 Cadastre-se Já
//               </button>
//               <div className="h-36 "></div>
//             </div>
//           </div>
//           <div className="flex justify-evenly text-center h-52 items-center ">
//             <div className="w-36 h-36 rounded-full shadow-md py-11 bg-white border-b-4 border-purple-300">
//               <p className="py-4">Praticidade</p>
//             </div>
//             <div className="w-36 h-36  rounded-full shadow-md py-11 bg-white   ">
//               <p className="py-4">Agilidade</p>
//             </div>
//             <div className="w-36 h-36  rounded-full shadow-md py-11 bg-white  ">
//               <p className="py-4">Multiplataforma</p>
//             </div>
//             <div className="  w-36 h-36  rounded-full shadow-md  py-11 bg-white ">
//               <p className="py-4">Elegância</p>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// }
