import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Api from "../../services/api";

export default function Login() {
  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = { nome, email, senha };
    console.log(data);
  };
  // const handleChange = (e) =>{

  //   setForm({
  //     ...form,
  //     [e.target.name]:[e.target.name]
  //   })

  // }
  return (
    <div className="  justify-center flex py-40 font-semibold h-full">
      <div className=" bg-white flex-col w-auto p-6 rounded-lg  py-8 px-8 shadow-lg">
        <form method="post" onSubmit={handleSubmit}>
          <div className="text-gray-600">Faça seu Login</div>
          <div className="h-4 "></div>
          <div>
            <TextField
              color="secondary"
              className="w-80 "
              id="outlined-basic"
              label="Email"
              variant="outlined"
              name="email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="h-4 "></div>
          <div>
            <TextField
              color="secondary"
              className="w-full"
              id="outlined-basic"
              label="Senha"
              variant="outlined"
              name="senha"
              type="password"
              value={senha}
              onChange={(e) => setSenha(e.target.value)}
            />
          </div>
          <div className="h-4 "></div>
          <div>
            <Button
              className="w-full"
              color="secondary"
              variant="contained"
              type="submit"
            >
              Entrar
            </Button>
            <div className="h-1 "></div>
            <Button
              className="w-full"
              color="secondary"
              variant="contained"
              type="submit"
            >
              Cadastre-se
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
}
