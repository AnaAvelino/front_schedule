import React, { useState } from "react";
import Header from "../../Components/Header/index";
// import Calendario from '../useCalendario/index'
// import Calendario from "react-cale";
// import "react-calendar/dist/Calendar.css";
import Perfil from "../../assets/perfil.jpeg";
import ImgCard from "../../assets/cardImg.png";
import Homefundo from "../../assets/fundoHome2.jpg";
import { Link } from "react-router-dom";
import { CardTitle } from "./CardTitle";

function classNames(...classes) {
  return classes.filter(Boolean).join("");
}

export default function Home() {
  const [value, onChange] = useState(new Date());
  // const Perfil = require("../../../public/perfil.jpeg");

  const infoHome = [
    {
      id: "1",
      title: "My Schedule",
      desc:
        "Fique a vontade para conhecer funcionalidades que o(a) ajudaram a organizar suas atividade diarias de um forma pratica e rápida",
      buttonTitle: "Cadastre-se Já",
      route: "/cad-usuario",
    },
  ];

  return (
    <div className="">
      <div className=" ">
        <div class="flex flex-col  ">
          <div
            className=" flex justify-center h-5/6 p-48 bg-gradient-to-r from-purple-500 to-pink-500 opacity-90"
            //={{ backgroundImage: `url(${Homefundo})` }}
          >
            <CardTitle dadoTitle={infoHome} />
          </div>
          <div className="h-9"></div>
          <div className="grid grid-cols-1 justify-items-center text-center h-52  md:grid-cols-1 lg:grid-cols-4">
            <div className="w-48 h-48 flex items-stretch justify-center rounded-md shadow-md  bg-white border-b-4 border-purple-300">
              <p className="self-center">Praticidade</p>
            </div>
            <div className="w-48 h-48 flex items-stretch justify-center   rounded-md shadow-md py-11 bg-white   ">
              <p className="self-center">Agilidade</p>
            </div>
            <div className="w-48 h-48 flex items-stretch justify-center   rounded-md shadow-md py-11 bg-white  ">
              <p className="self-center">Multiplataforma</p>
            </div>
            <div className="w-48 h-48 flex items-stretch justify-center   rounded-md shadow-md  py-11 bg-white ">
              <p className="self-center">Elegância</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
