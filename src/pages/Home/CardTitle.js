import React, { useState } from "react";
import { Link } from "react-router-dom";

export function CardTitle({ dadoTitle }) {
  const [state, setState] = useState(dadoTitle);
  console.log("-------------", dadoTitle);

  return (
    <div className=" flex flex-col bg-gray-50 opacity-90 w-2/5 p-5 text-center rounded-lg md:w-80">
      {state.map((value) => {
        return (
          <>
            <h1 className="titleLogo mt-10 ">{value.title}</h1>
            <div className="h-4"></div>
            <p className="text font-semibold">{value.desc}</p>
            <div className="h-5"></div>
            <div>
              <Link
                className="button text-lg font-semibold rounded-md shadow-md bg-purple-900 text-gray-200 w-46 h-25 p-2 max-w-14"
                to={value.route}
              >
                {value.buttonTitle}
              </Link>
            </div>
            <div className="h-4"></div>
          </>
        );
      })}
      {/* <div className="h-36 "></div> */}
    </div>
  );
}
