import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import * as Yup from "yup";
import { Form, Field } from "react-final-form";
import { Link } from "react-router-dom";
import Button from "../../Components/Button";
import Api from "../../services/api";
import api from "../../services/api";

export default function CadastroUsuarios() {
  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");
  const [senhaConfimacao, setSenhaConfimacao] = useState("");
  const [msg, setMsg] = useState("");

  const validationSchema = Yup.object().shape({
    nome: Yup.string().required(),
  });

  // schema
  // .isValid({})
  const dados = { nome, email, senha, senhaConfimacao };

  // schema
  // .isValid(dados)
  // .then((valid) => {
  //   console.log("Valor válido:", valid);
  // }).catch(erro => {
  //   console.log("ERRRRRRRO", erro)
  // });

  const onSubmit = async (e) => {
    var array = [];
    console.log("aaaaaaaaaaaaaaaaaaaaaaaa", e);

    // e.preventDefault();
    // const dados = { nome, email, senha, senhaConfimacao };
    // console.log(dados);
    console.log("API", api);
    api
      .post("/users", e)
      .then((res) => {
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const validate = (values) => {
    const errors = {};
    if (!values.nome) {
      errors.nome = "Campo Obrigatorio";
    }
    if (!values.email) {
      errors.email = "Campo Obrigatorio";
    }
    if (!values.senha) {
      errors.senha = "Campo Obrigatorio";
    } else if (values.senha.length < 6) {
      errors.senha = "Mínimo de 6 caracteres";
    }
    // if (!values.senhaConfimacao) {
    //   errors.senhaConfimacao = "Required";
    // }
    // if (!values.confirm) {
    //   errors.confirm = "Required";
    // } else if (values.confirm !== values.password) {
    //   errors.confirm = "Must match";
    // }
    // console.log("sssssss", Object.keys(errors).length);
    // console.log(errors);
    return errors;
  };

  return (
    <div className=" justify-center flex py-14  body ">
      <div className=" flex-col w-auto p-6 rounded-lg py-8 px-8 shadow-lg  bg-white text">
        <Form onSubmit={onSubmit} validate={validate}>
          {(props) => {
            // console.log(props.handleSubmit);
            return (
              <form onSubmit={props.handleSubmit}>
                <div className="text-gray-600 text-xl text-center m-3 mb-4 font-semibold">
                  Cadastro de Usuario
                </div>
                <div className="h-7 "></div>
                <div>
                  <Field name="nome">
                    {({ input, meta }) => {
                      return (
                        <div>
                          <TextField
                            error={meta.error && meta.touched ? true : false}
                            color="secondary"
                            className="w-80 text-md"
                            id="outlined-basic"
                            label="Nome"
                            variant="outlined"
                            {...input}
                            helperText={
                              meta.error && meta.touched && meta.error
                            }
                          />
                        </div>
                      );
                    }}
                  </Field>
                  <div className="h-4 "></div>
                  <Field name="email">
                    {({ input, meta }) => {
                      return (
                        <div>
                          <TextField
                            error={meta.error && meta.touched ? true : false}
                            color="secondary"
                            className="w-80 "
                            id="outlined-basic"
                            label="Email"
                            variant="outlined"
                            type="email"
                            {...input}
                            helperText={
                              meta.error && meta.touched && meta.error
                            }
                          />
                        </div>
                      );
                    }}
                  </Field>
                  <div className="h-4 "></div>
                  <Field name="senha">
                    {({ input, meta }) => {
                      return (
                        <div>
                          <TextField
                            size="medium"
                            error={meta.error && meta.touched ? true : false}
                            color="secondary"
                            className="w-full"
                            id="outlined-basic"
                            label="Senha"
                            variant="outlined"
                            type="password"
                            {...input}
                            helperText={
                              meta.error && meta.touched && meta.error
                            }
                          />
                        </div>
                      );
                    }}
                  </Field>
                  {/* <div className="h-4 "></div>
                  <Field name="senhaConfimacao">
                    {({ input, meta }) => {
                      return (
                        <div>
                          <TextField
                            error={meta.error && meta.touched ? true : false}
                            color="secondary"
                            className="w-full"
                            id="outlined-basic"
                            label="Senha"
                            variant="outlined"
                            type="password"
                            {...input}
                            helperText={
                              meta.error && meta.touched && meta.error
                            }
                          />
                        </div>
                      );
                    }}
                  </Field> */}
                  <div className="h-4 "></div>
                  <Button type="submit" title="Salvar"/>
                </div>
                <div className="h-7 "></div>
                <div className="flex justify-center">
                  Já possui conta?{" "}
                  <Link
                    to="/login"
                    className="text-purple-700 font-semibold pl-2 "
                  >
                    Entre
                  </Link>
                </div>
              </form>
            );
          }}
        </Form>
      </div>
    </div>
  );
}
